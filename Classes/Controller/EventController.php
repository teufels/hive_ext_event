<?php
namespace HIVE\HiveExtEvent\Controller;

/***
 *
 * This file is part of the "hive_ext_event" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * EventController
 */
class EventController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * eventRepository
     *
     * @var \HIVE\HiveExtEvent\Domain\Repository\EventRepository
     * @inject
     */
    protected $eventRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $events = $this->eventRepository->findAll();
        $this->view->assign('events', $events);
    }

    /**
     * action show
     *
     * @param \HIVE\HiveExtEvent\Domain\Model\Event $event
     * @return void
     */
    public function showAction(\HIVE\HiveExtEvent\Domain\Model\Event $event)
    {
        $this->view->assign('event', $event);
    }
}
