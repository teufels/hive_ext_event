<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_event', 'Configuration/TypoScript', 'hive_ext_event');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextevent_domain_model_event', 'EXT:hive_ext_event/Resources/Private/Language/locallang_csh_tx_hiveextevent_domain_model_event.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextevent_domain_model_event');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_event',
            'tx_hiveextevent_domain_model_event'
        );

    }
);
